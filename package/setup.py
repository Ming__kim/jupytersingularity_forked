#!/usr/bin/env python3

from setuptools import setup, find_packages

# https://packaging.python.org/tutorials/packaging-projects/
with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="PackageName",
    version="0.0.1",
    install_requires=[],
    include_package_data=True,
    py_modules = ['exe'],
    
# Metadata
    author="Young-Chan Park",
    author_email="young.chan.park93@gmail.com",
    description="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://www.packagename.com",
    packages = find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: CentOS 7.6"
        #"License :: OSI Approved :: Python Software Foundation License"
    ],
    python_requires='>=3.6',
)
